# Changelog

## Unreleased

## Added

- Installable en tant que package Composer

## Changed

- Compatible SPIP 5.0.0-dev

## Removed

- Drop support for PHP7.4 & PHP8.0
